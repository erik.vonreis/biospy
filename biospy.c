#include <unistd.h>
#include <fcntl.h>
#include <inttypes.h>
#include <dirent.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <time.h>
#include <signal.h>

struct CARD_TYPE {
	uint16_t vendor;
	uint16_t device;
	size_t size;
	char name[256];	
};

struct CARD_TYPE card_types[] = {{0x1221, 0x8682, 32, "Contec DIO-64"}, {0,0,0,""}};

char pci_device_dir[] = "/sys/bus/pci/devices";

struct PCI_ADDR {
	uint16_t a;
	uint16_t b;
	uint16_t c;
	uint16_t d;
};

struct CARD {
	struct CARD_TYPE *type;
	uint16_t port_address;
	struct PCI_ADDR pci_addr;
	char path[256];
};

/// compare CARD structs by pci_addr
/// for qsort
int cmp_card_pci_addr(const void *card1_v, const void *card2_v)
{
	const struct CARD *card1 = (struct CARD *)card1_v;
	const struct CARD *card2 = (struct CARD *)card2_v;
	const struct PCI_ADDR *pa1 = &card1->pci_addr;
	const struct PCI_ADDR *pa2 = &card2->pci_addr;


	if(pa1->a < pa2->a) return -1;
	if(pa1->a > pa2->a) return 1;
	if(pa1->b < pa2->b) return -1;
	if(pa1->b > pa2->b) return 1;
	if(pa1->c < pa2->c) return -1;
	if(pa1->c > pa2->c) return 1;
	if(pa1->d < pa2->d) return -1;
	if(pa1->d > pa2->d) return 1;
	return 0;
	
}

#define MAXCARDS 32
struct CARD cards[MAXCARDS];

int card_count=0;

void summarize_cards()
{
	for(int i=0; i < card_count; ++i) 
	{
		struct CARD *card = &cards[i];
		printf("Card %d is a %s. PCI addr = %x.  I/O port address = 0x%hx\n", 
				i+1, 
				card->type->name, 
				card->pci_addr.b, card->port_address);
	}
}

/// check if the device pointed at by dname
/// is one of the known card types
/// if so add it to the cards array
int check_device(const char *dname)
{
	char full_path[512];
	struct PCI_ADDR pci_addr;

	int numread = sscanf(dname, "%hx:%hx:%hx.%hx", &pci_addr.a, &pci_addr.b, &pci_addr.c, &pci_addr.d);
	//printf("%s scanned %d values to %x %x %x %x\n", dname, numread, pci_addr.a,pci_addr.b,pci_addr.c,pci_addr.d);

	snprintf(full_path, 512, "%s/%s", pci_device_dir, dname);
	full_path[511] = 0;

	chdir(full_path);

	uint16_t device=0;
	uint16_t vendor=13;

	// read vendor and device values
	FILE *vendor_file = fopen("vendor","rt");
	if(NULL != vendor_file)
	{
		char venstr[15];
		//fscanf(vendor_file, "%x", &vendor);
		fscanf(vendor_file, "%s", venstr);
		vendor = strtol(venstr+2, NULL, 16);
		fclose(vendor_file);
	}
	else
	{
		printf("could not read vendor from %s\n", dname);
	}

	FILE *device_file = fopen("device","rt");
	if(NULL != device_file)
	{
		fscanf(device_file, "%x", &device);
		fclose(device_file);
	}
	else
	{
		printf("could not read device form %s\n", dname);
	}

	int cardtype=0;
	while(card_types[cardtype].name[0] != 0)
	{
		if((card_types[cardtype].vendor == vendor) && (card_types[cardtype].device == device))
		{
			if(card_count < MAXCARDS)
			{
				cards[card_count].pci_addr = pci_addr;
				cards[card_count].type = card_types + cardtype;
				strncpy(cards[card_count].path, full_path, sizeof(cards[0].path));
				cards[card_count].path[sizeof(cards[0].path)-1] = 0;
			

				FILE *res_f = fopen("resource", "rt");
				if(NULL != res_f)
				{
					uint16_t port_add;
					fscanf(res_f, "%hx", &port_add);
					fclose(res_f);
					cards[card_count].port_address = port_add;
					int k = ioperm(port_add, cards[card_count].type->size * sizeof(char), 1);
					if(0 == k)
					{
						++card_count;
					}
					else
					{
						printf("failed to get permission to write IO ports at 0x%hx\n", port_add);
					}
				}
				else
				{
					printf("could not open resource file for %s\n", dname);
				}

			}
			else
			{
				printf("A %s was found but the maximum card count was exceeded.\n", card_types[cardtype].name);
			}

			break;
		}

		++cardtype;
	}

}

int flow_display()
{
	long long inregs, outregs;
 	for(int i=0;i<card_count;++i)
	{
		struct CARD *card = &cards[i];
		inregs = inl(card->port_address);
		outregs = inl(card->port_address+8);
		printf("%lx %lx ", inregs, outregs);
	}
	printf("\n");
}


int monitor_loop(int rate_ms)
{
	struct timespec sleep;
	sleep.tv_sec = 0;
	sleep.tv_nsec = rate_ms * 1000000;


	for(;;)
	{
		nanosleep(&sleep, NULL);
		flow_display();		
	}
}

void close_cards(void)
{
	printf("cleaning up cards\n");
	for(int i=0; i<card_count; ++i)
	{
		struct CARD *card = &cards[i];
		int k = ioperm(card->port_address, card->type->size * sizeof(char), 0);
		if(0 != k)
		{
			printf("card %d: could not relinquish permission for IO ports at 0x%hx\n", i, card->port_address);
		}
	}
}

void sig_handler(int signum)
{
	printf("Exiting on signal %d\n", signum);
	exit(0);
}

int main(void)
{
	signal(SIGTERM, sig_handler);
	signal(SIGINT, sig_handler);

	// scan for binary IO cards
	DIR *devrootdir = opendir(pci_device_dir);
	atexit(&close_cards);
	if(NULL == devrootdir)
	{
		printf("failed to open pci device directory: %d\n", errno);
		return 1;
	}
	for(;;)
	{
		struct dirent *devdir = readdir(devrootdir);

		if(NULL == devdir) break;
		if('.' == devdir->d_name[0]) continue;


		check_device(devdir->d_name);
	}
	closedir(devrootdir);

	// monitoring
	
	if(card_count == 0)
	{
		printf("no bio cards were found to monitor! exiting\n");
		return 2;
	}

	qsort(cards, card_count, sizeof(cards[0]), &cmp_card_pci_addr);
	summarize_cards();

	monitor_loop(750);



}
